import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MenuService } from 'src/app/services/menu/menu.service';
import { IMenuItem } from 'src/app/core/models/IMenuItem';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-aside-menu',
  templateUrl: './aside-menu.component.html',
  styleUrls: ['./aside-menu.component.css']
})
export class AsideMenuComponent implements OnInit, AfterViewInit {
  constructor(private menuService: MenuService, private authService: AuthService) { }

  DataModel: IMenuItem[]; // Array<IMenuItem>
  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.authService.onAuthStatusChanged.subscribe(result => {
      if (result && !this.DataModel) {
        this.menuService.findAll().pipe(take(1))
          .subscribe(menuData => this.DataModel = menuData.data);
      }
    });
  }

}
