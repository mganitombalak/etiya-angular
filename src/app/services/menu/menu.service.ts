import { Injectable } from '@angular/core';
import { IBaseResponse } from 'src/app/core/models/IBaseResponse';
import { IMenuItem } from 'src/app/core/models/IMenuItem';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import BaseService from 'src/app/core/base/base-service';

@Injectable({ providedIn: 'root' })
export class MenuService extends BaseService<IMenuItem> {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
    this.endpoint = 'menu';
  }

}
