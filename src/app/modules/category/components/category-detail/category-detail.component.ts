import { Component, OnInit } from '@angular/core';
import { IBaseResponse } from 'src/app/core/models/IBaseResponse';
import { Observable } from 'rxjs';
import { ICategory } from 'src/app/core/models/ICategory';
import { BaseModalComponent } from 'src/app/core/base/base-moda-component';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent extends BaseModalComponent<ICategory> implements OnInit {

  constructor() { super();}
  ngOnInit() {
    this.componentMode = ComponentMode.Edit;
  }
  onUpsertSubmit() {
    let serviceObservable: Observable<IBaseResponse<ICategory>>;
  }
}
