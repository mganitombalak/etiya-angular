import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CattegoryListComponent } from './cattegory-list.component';

describe('CattegoryListComponent', () => {
  let component: CattegoryListComponent;
  let fixture: ComponentFixture<CattegoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CattegoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CattegoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
