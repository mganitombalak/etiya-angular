import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ICategory } from 'src/app/core/models/ICategory';
import { CategoryService } from '../../services/category.service';
import { take } from 'rxjs/operators';
import { GridCellComponent } from 'src/app/modules/shared/components/grid-cell/grid-cell.component';
import { ModalService } from 'src/app/modules/shared/services/modal.service';
import { CategoryDetailComponent } from '../category-detail/category-detail.component';

@Component({
  selector: 'app-cattegory-list',
  templateUrl: './cattegory-list.component.html',
  styleUrls: ['./cattegory-list.component.css']
})
export class CattegoryListComponent implements OnInit, AfterViewInit {
  columnDefs = [
    { headerName: 'Name', field: 'name' },
    { headerName: 'Display Order', field: 'displayOrder' },
    { headerName: 'Created At', field: 'createdAt' },
    { headerName: 'IsActive', field: 'isActive' },
    { headerName: 'Action', width: 250, cellRendererFramework: GridCellComponent }
  ];

  DataModel: Array<ICategory>;
  Context: any;
  constructor(private categoryService: CategoryService, private modalService: ModalService) { }

  ngOnInit() {
    this.Context = { parent: this };
  }

  ngAfterViewInit(): void {
    this.categoryService.findAll().pipe(take(1)).subscribe(result => this.DataModel = result.data);
  }

  onEdit(data: ICategory) {
    this.modalService.open(
      {
        title: `Edit ${data.name}`,
        componentMode: ComponentMode.Edit,
        activeComponent: CategoryDetailComponent,
        data
      });
  }

  onDelete(data: ICategory) {

  }

  onInfo(data: ICategory) {

  }
}
