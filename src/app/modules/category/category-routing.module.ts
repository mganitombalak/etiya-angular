import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CattegoryListComponent } from './components/cattegory-list/cattegory-list.component';


const routes: Routes = [
  { path: '', component: CattegoryListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
