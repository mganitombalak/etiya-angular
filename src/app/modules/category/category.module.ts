import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CattegoryListComponent } from './components/cattegory-list/cattegory-list.component';
import { AgGridModule } from 'ag-grid-angular';
import { CategoryService } from './services/category.service';
import { SharedModule } from '../shared/shared.module';
import { GridCellComponent } from '../shared/components/grid-cell/grid-cell.component';
import { CategoryDetailComponent } from './components/category-detail/category-detail.component';

@NgModule({
  declarations: [CattegoryListComponent, CategoryDetailComponent],
  imports: [
    SharedModule,
    CategoryRoutingModule,
    AgGridModule.withComponents([GridCellComponent])
  ],
  providers: [CategoryService],
  entryComponents: [CategoryDetailComponent]
})
export class CategoryModule { }
