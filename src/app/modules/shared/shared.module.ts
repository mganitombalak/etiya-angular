import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridCellComponent } from './components/grid-cell/grid-cell.component';
import { ModalComponent } from './components/modal/modal.component';
import { ModalBodyContainerDirective } from 'src/app/core/directives/modal-body-container.directive';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [GridCellComponent, ModalComponent, ModalBodyContainerDirective],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CommonModule, FormsModule, GridCellComponent, ModalComponent, ModalBodyContainerDirective]
})
export class SharedModule { }
