import { Component, OnInit } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams, IAfterGuiAttachedParams } from 'ag-grid-community';

@Component({
  selector: 'app-grid-cell',
  templateUrl: './grid-cell.component.html',
  styleUrls: ['./grid-cell.component.css']
})
export class GridCellComponent implements AgRendererComponent {
  private params: any;
  refresh(params: any): boolean {
    return false;
  }

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
    this.params = params;
  }

  onEditClicked() {
    this.params.context.parent.onEdit(this.params.data);
  }

  onDeleteClicked() {
    this.params.context.parent.onDelete(this.params.data);
  }
  onInfoClicked() {
    this.params.context.parent.onInfo(this.params.data);
  }
}