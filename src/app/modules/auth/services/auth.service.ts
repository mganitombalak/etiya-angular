import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { IAuthRequest } from 'src/app/core/models/IAuthRequest';
import { take } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { IAuthResponse } from 'src/app/core/models/IAuthResponse';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class AuthService {
  onAuthStatusChanged: BehaviorSubject<boolean>;
  onAuthError: BehaviorSubject<boolean>;
  constructor(private httpClient: HttpClient, private toast: ToastrService, private router: Router) {
    this.onAuthStatusChanged = new BehaviorSubject(this.isAuthenticated() || false);
    this.onAuthError = new BehaviorSubject(null);
  }

  login(loginInfo: IAuthRequest): void {
    const auth = this.httpClient.post('auth', JSON.stringify(loginInfo)) as Observable<IAuthResponse>;
    auth.pipe(take(1)).subscribe((res) => {
      localStorage.setItem('userData', JSON.stringify(res.data));
      localStorage.setItem('token', res.token);
      this.onAuthStatusChanged.next(true);
      this.router.navigate(['/']);
    }, e => { this.onAuthError.next(e); this.toast.error('Login Failed'); });
  }

  logout(): void {
    localStorage.clear();
    this.onAuthStatusChanged.next(false);
    this.router.navigate(['/auth']);
  }

  isAuthenticated = () => !!localStorage.getItem('token');
}
