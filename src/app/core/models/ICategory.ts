export interface ICategory {
    id: string;
    name: string;
    displayOrder: number;
    isActive: boolean;
    createdAt: string;
}