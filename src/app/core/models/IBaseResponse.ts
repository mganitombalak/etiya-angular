export interface IBaseResponse<T> {
    totalRecordNumber: number;
    data: Array<T>;
    humanReadableMessage: Array<string>;
}