export class BaseModalComponent<T> {
    model: T;
    componentMode: ComponentMode;
    actionButtonText: string;
    bind(options: any): void {
        this.model = options.data ? options.data : {} as T;
        this.componentMode = options.componentMode;
        this.actionButtonText =
            this.componentMode === ComponentMode.Insert ? 'Save' :
                this.componentMode === ComponentMode.Edit ? 'Update' :
                    this.componentMode === ComponentMode.Delete ? 'Delete' : '';
    }
}