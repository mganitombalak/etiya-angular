import { Observable } from 'rxjs';
import { IBaseResponse } from '../models/IBaseResponse';
import { HttpClient } from '@angular/common/http';

export default class BaseService<T> {

    protected endpoint: string;
    constructor(protected httpClient: HttpClient) { }

    findAll(): Observable<IBaseResponse<T>> {
        return this.httpClient.get(this.endpoint) as Observable<IBaseResponse<T>>;
    }

    deleteById(id: string): void {

    }

    findById(id: string): Observable<IBaseResponse<T>> {
        return null;
    }
}