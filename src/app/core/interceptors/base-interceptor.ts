import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable()
export class BaseInterceptor implements HttpInterceptor {
    constructor() { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let requestObject = req.clone({
            url: `${environment.baseUrl + req.url}`,
            headers: req.headers
                .set('vendor', 'etiya')
                .set('Access-Control-Allow-Origin', '*')
                .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
        });
        if (!(req.body instanceof FormData)) {
            requestObject = requestObject.clone({ headers: requestObject.headers.append('Content-Type', 'application/json') });
        }
        return next.handle(requestObject);
    }
}
