import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnHoverActive]'
})
export class OnHoverActiveDirective {

  constructor(private element: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter', null) OnMouseEnter() {
    this.renderer.addClass(this.element.nativeElement, 'active');
  }
  @HostListener('mouseleave', null) OnMouseLeave() {
    this.renderer.removeClass(this.element.nativeElement, 'active');
  }
}
