import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appMenuHover]'
})
export class MenuHoverDirective {

  private subDivContainer: any;
  constructor(private element: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter', ['$event']) OnMouserEnter(event: Event) {
    this.subDivContainer = this.element.nativeElement.querySelector('.dropdown-menu');
    this.renderer.addClass(this.subDivContainer, 'show');
    // console.log(this.element.nativeElement.id + ' mouse entered');
  }

  @HostListener('mouseleave', ['$event']) OnMouserLeave(event: Event) {
    this.subDivContainer = this.element.nativeElement.querySelector('.dropdown-menu');
    this.renderer.removeClass(this.subDivContainer, 'show');
    // console.log(this.element.nativeElement.id + ' mouse left');
  }

}
